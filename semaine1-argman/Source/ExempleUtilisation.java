
// Voici un exemple de "programme" qui utiliserait les fonctionnalités du ArgMan
public class ExempleUtilisation {

    // Vous devriez passer trois parametres au programme pour que l'exemple fonctionne:
    // -x entier [-y] [-z string]     (par exemple: -x 5 -z allo)
    public static void main(String[] args)
    {
        
        // Cette premiere ligne indique au ArgMan la configuration des parametres qu'il doit 
        // reconnaitre et valider (ie. le schéma).
        // En ce moment par contre, on initialize seulement un ArgManStub, qui ne fera rien d'utile 
        // avec les arguments qu'il recevra.
        ArgMan argman = new ArgManStub("x#,y$,z*?", args);
        
        // Tout le reste du travail du ArgMan se fait ensuite de manière invisible pour le programme
        // hôte. En fait, il n'a plus qu'à "questionner" le ArgMan pour accéder aux arguments.
        // L'interface du ArgMan permet au programme d'acceder à la valeur de chaque argument selon
        // un type spécifique.
        int x = argman.getInt('x');
        boolean y = argman.getBoolean('y');
        String z = argman.getString('z');

        // Le reste du programme fictif est simplement representé par un appel à "doSomething" ici.
        doSomething(x, y, z);
    }

    // Dans une situation réelle, ceci serait le reste du programme...
    private static void doSomething (int a, boolean b, String c)
    {
        cout ("entier: " + a);
        cout("booleen: " + b);
        cout("chaine: " + c);
    }

    private static void cout (String message) {
        System.out.println(message);
    }
}
