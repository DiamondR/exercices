import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author David Giasson <david.giasson@claurendeau.qc.ca>
 */
public class ArgManTest2 {
    
    private final String schema = "a#?,b#?,c*?,d*?";
    private final String sampleArgs = "-a 1 -b 2 -c hello -d world";
    
    private ArgMan argman;
    
    @Before
    public void setUp() throws Exception {
        argman = new ArgManStub(schema);
    }

    @Test
    public void testAllArguments() throws Exception {
        loadArgs(sampleArgs);
        verify(1, 2, "hello", "world");
    }
    
    @Test
    public void testOrderInvariant() throws Exception {
        loadArgs("-d world -b 4 -a 3 -c hello");
        verify(3, 4, "hello", "world");
    }
    
    @Test
    public void testOptional1() throws Exception {
        loadArgs("-b 6 -d world -a 5");
        verify(5, 6, "", "world") ;
    }
    
    @Test
    public void testOptional2() throws Exception {
        loadArgs("-c hello");
        verify(0, 0, "hello", "");
    }
    
    @Test
    public void testMinimumArguments() throws Exception {
        argman.loadArgs(new String[0]); // this.loadArgs would pass {""} instead of {}
        verify(0, 0, "", "");
    }
    
    @Test (expected = Exception.class)
    public void testMissingValue1() throws Exception {
        loadArgs("-a 1 -b -c hello -d world");
    }
    
    @Test (expected = Exception.class)
    public void testMissingValue2() throws Exception {
        loadArgs("-a 1 -b 2 -c -d world");
    }
    
    @Test (expected = Exception.class)
    public void testUnexpectedValue() throws Exception {
        loadArgs("-a 1 -b 2 cheese -c hello -d world");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidValueType1() throws Exception {
        loadArgs("-a 1 -b cheese -c hello -d world");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidValueType2() throws Exception {
        loadArgs("-a 1 -b 2.2 -c hello -d world");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidSyntax() throws Exception {
        loadArgs("-a 1 -b2 -c hello -d world");
    }
    
    @Test (expected = Exception.class)
    public void testQueryWrongType1() throws Exception {
        loadArgs(sampleArgs);
        argman.getBoolean('a');
    }
    
    @Test (expected = Exception.class)
    public void testQueryWrongType2() throws Exception {
        loadArgs(sampleArgs);
        argman.getInt('d');
    }
    
    @Test
    public void testArgsReloaded() throws Exception {
        // this test is optional (success is preferable but failure is acceptable)
        loadArgs(sampleArgs);
        loadArgs("-b 9 -c cheese");
        verify(0, 9, "cheese", "");
    }
    
    private void loadArgs(String rawArgs) throws Exception {
        argman.loadArgs(rawArgs.split(" "));
    }
    
    private void verify(int a, int b, String c, String d) throws Exception {
        assertEquals(a, argman.getInt('a'));
        assertEquals(b, argman.getInt('b'));
        assertEquals(c, argman.getString('c'));
        assertEquals(d, argman.getString('d'));
    }
}
