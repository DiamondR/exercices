import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author David Giasson david.giasson@claurendeau.qc.ca
 */
public class ArgManTest3 {
    
    private ArgMan argman;
    
    @Test
    public void testSimpleSchema() throws Exception {
        argman = new ArgManStub("x*?,y$,z#");
        argman.loadArgs("-z 5 -y -x hello".split(" "));
        assertEquals(5, argman.getInt('z'));
        assertEquals(true, argman.getBoolean('y'));
        assertEquals("hello", argman.getString('x'));
    }
    
    @Test
    public void testComplexSchema() throws Exception {
        argman = new ArgManStub("x*?,y$,z#,a$,b*,c#?");
        argman.loadArgs("-z 5 -a -x hello -b world".split(" "));
        assertEquals(5, argman.getInt('z'));
        assertEquals(false, argman.getBoolean('y'));
        assertEquals("hello", argman.getString('x'));
        assertEquals(true, argman.getBoolean('a'));
        assertEquals("world", argman.getString('b'));
        assertEquals(0, argman.getInt('c'));
    }
    
    @Test
    public void testEmptySchema() throws Exception {
        argman = new ArgManStub("");
        argman.loadArgs(new String[0]);
    }
    
    @Test (expected = Exception.class)
    public void testInvalidSyntax() throws Exception {
        argman = new ArgManStub("x*?,cheese,z#");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidSeparator() throws Exception {
        argman = new ArgManStub("x*?-y$-z#");
    }
    
    @Test (expected = Exception.class)
    public void testInvalidParameter() throws Exception {
        argman = new ArgManStub("x*?,0$,z#");
    }
    
    @Test (expected = Exception.class)
    public void testMissingParameter() throws Exception {
        argman = new ArgManStub("x*?,$,z#");
    }
    
    @Test (expected = Exception.class)
    public void testMissingType() throws Exception {
        argman = new ArgManStub("x*?,y,z#");
    }
    
    @Test (expected = Exception.class)
    public void testUnknownType() throws Exception {
        argman = new ArgManStub("x*?,y%,z#");
    }
    
    @Test (expected = Exception.class)
    public void testModifierType() throws Exception {
        argman = new ArgManStub("x*?,y?,z#");
    }
    
    @Test (expected = Exception.class)
    public void testConflictingType() throws Exception {
        argman = new ArgManStub("x*?,y$*,z#");
    }
    
    @Test (expected = Exception.class)
    public void testPermutation1() throws Exception {
        argman = new ArgManStub("x*?,#z");
    }
    
    @Test (expected = Exception.class)
    public void testPermutation2() throws Exception {
        argman = new ArgManStub("x*?,?z#");
    }
    
    @Test (expected = Exception.class)
    public void testPermutation3() throws Exception {
        argman = new ArgManStub("x*?,z?#");
    }
    
    @Test (expected = Exception.class)
    public void testSchemaIncomplete1() throws Exception {
        argman = new ArgManStub("x*?,,z?#");
    }
    
    @Ignore 
    @Test (expected = Exception.class)
    public void testSchemaIncomplete2() throws Exception {
        argman = new ArgManStub("x*?,");
    }
    
    @Test (expected = Exception.class)
    public void testSchemaIncomplete3() throws Exception {
        argman = new ArgManStub(",x*?");
    }
}
